function y=myfun2(coeff,x)
a=coeff(1);
b=coeff(2);
c=coeff(3);
d=coeff(4);
y=a*exp(b*x)+c*exp(d*x);
end
