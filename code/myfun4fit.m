function f = myfun(coeffs,xdata)
%MYFUN 此处显示有关此函数的摘要
%   此处显示详细说明
a=coeffs(1);
b=coeffs(2);
c=coeffs(3);
d=coeffs(4);
x=xdata.x;
y=xdata.y;
f=a+b./(c+x.*y./(d*x+(1-d)*y));
end

