%% using ipopt
usingold=0;
if usingold
mypath2='UmassGreenComputingSP2013Project-master\src\opti\Solvers\ipopt';
addpath(mypath2)
end
% a folder contianing algs which can be used to cal the gradient
% a function;
% mypath='madiff-master';
% addpath(mypath);
% f_grad = @(x)adiff(f, x);
% g = f_grad(x);


funcs.objective=f;
funcs.gradient=f_grad;
% options.lb=[-Inf -Inf -Inf -Inf];
% options.ub=[Inf Inf Inf Inf];
% 1.1688   -3.0592    7.7677    0.5574
options.lb=[0 -10 0 0];
options.ub=[5 0 10 1];
% options.cl=+Inf;
% options.cu=-Inf;

options.ipopt.max_iter = 200; % max. number of iterations
options.ipopt.tol = 1e-7; % accuracy to which the NLP is solved
options.ipopt.print_level = 5; % {6}, [5] is a nice compact form

options.ipopt.hessian_approximation = 'limited-memory';
options.ipopt.limited_memory_update_type = 'bfgs'; % {bfgs}, sr1
options.ipopt.limited_memory_max_history = 6; % {6}

options.ipopt.jac_c_constant        = 'yes';

% % Barrier strategy
options.ipopt.mu_strategy = 'adaptive'; % monotone (Fiacco-McCormick), {adaptive}
options.ipopt.mu_oracle = 'probing'; % {quality-function}, loqo, [probing


[x,info]=ipopt(x0, funcs, options);
myipopt=x
% rmpath(mypath);
if usingold
rmpath(mypath2)
end
