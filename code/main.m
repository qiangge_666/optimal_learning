clear
clc
close all
[xdata,ydata]=preparedata;
%% obj function handle
f=@(x)myfun(x,xdata,ydata);
% initial guess
x0=[1 1 1 1];
f_grad=@(x)myfun_grad(x,xdata,ydata);

%% using matlab fmincon
if 1
[X,FVAL,EXITFLAG,OUTPUT,LAMBDA,GRAD,HESSIAN] = fmincon(f,x0);
myfmincon=X
% [X,FVAL,EXITFLAG,OUTPUT] = opti_fmincon(f,x0);
% myopti_fmincon=X

[X,FVAL,EXITFLAG,OUTPUT] = fminsearch(f,x0);
myfminsearch=X
% optfminunc = optimoptions(@fminunc,'Display','iter','Algorithm','quasi-newton');
optfminunc = optimoptions(@fminunc,'Algorithm','quasi-newton');
[X,FVAL,EXITFLAG,OUTPUT] = fminunc(f,x0,optfminunc);
myfminunc=X
[X,FVAL,EXITFLAG,OUTPUT] = lsqcurvefit('myfun4fit',x0,xdata,ydata);
mylsqcurvefit=X

run_nlopt
run_levmar_fit
run_nl2sol
run_nomad
run_opti_fmincon
run_ipopt
run_lbfgsb
end

% run_lsqnonlin



