oldoptions = optimoptions(@lsqnonlin,'Algorithm','levenberg-marquardt',...
    'MaxFunctionEvaluations',500000,'MaxIterations',500000,'UseParallel',0)
[x,resnorm,residual,exitflag,output,lambda,jacobian]= lsqnonlin(f,x0,[],[],oldoptions);
mylsqnonlin=x