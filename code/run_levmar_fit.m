%% using levmar
% [x,fval,exitflag,iter] = levmar(fun,grad,x0,ydata,lb,ub,A,b,Aeq,beq,opts)
mypath3='UmassGreenComputingSP2013Project-master\src\opti\Solvers\levmar';
addpath(mypath3)
% [x,fval,exitflag,iter] = levmar(f,f_grad,x0,ydata)
[x,fval,exitflag,iter] = levmar(@(x)myfun4fit(x,xdata),[],x0,ydata);
mylevmar=x'
rmpath(mypath3)