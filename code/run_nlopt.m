%% using nlopt
if 1
mypath2='UmassGreenComputingSP2013Project-master\src\opti\Solvers\nlopt';
addpath(mypath2)
% opt.min_objective=f;
% opt.min_objective=@(x)myfun(x,xdata,ydata);
opt.objective=@(x)myfun(x,xdata,ydata);
if 0
%% NLOPT_LN_BOBYQA
% 1.1378   -1.9431    4.2410    0.5515
opt.options.algorithm = 34
%% NLOPT_LN_COBYLA
% 1.1041   -1.1826    2.0061    0.5569
opt.options.algorithm = 25
%% NLOPT_LN_NEWUOA
% 1.1119   -1.2217    1.6303    0.5485
opt.options.algorithm = 26
%% NLOPT_LN_NEWUOA_BOUND
% 1     0     1     1
opt.options.algorithm = 27 %wrong
%% NLOPT_LN_NELDERMEAD
% 1.1688   -3.0601    7.7706    0.5574
opt.options.algorithm = 28 %
%% NLOPT_LN_SBPLX
% 1.1251   -1.6610    3.4706    0.5538
opt.options.algorithm = 29 %

end
%% NLOPT_LN_PRAXIS
%best 1.1688   -3.0592    7.7678    0.5574
opt.options.algorithm = 12 ;
%% gradient method
% opt.gradient=f_grad;
% opt.options.algorithm = 40;%
%% 
% opt.verbose = 1;
opt.xtol_rel = 1e-8;

opt.lb=[-Inf -Inf -Inf -Inf];
opt.ub=[Inf Inf Inf Inf];
% nlprob.options.algorithm=40
[xopt, fopt, retcode,funcevals] = nlopt(opt, x0);
mynlopt=xopt'
% [xopt, fopt, retcode] = opti_nlopt(opt, x0)

rmpath(mypath2)
end
