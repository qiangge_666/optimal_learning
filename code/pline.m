function yy=pline(x,y)
nx=length(x);ny=length(y); %分别求向量x,y的长度
n=min(nx,ny); %求出长度的最小值
x=reshape(x,n,1); %生成列向量
y=reshape(y,n,1);
M=[x ones(n,1)]; %连接矩阵M
B=y;
N=M'*M;
B=M'*B;
yy=N\B; %得到拟合系数
yy=yy'; %变成行向量

end

