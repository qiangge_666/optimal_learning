function f_grad = myfun(coeffs,xdata,ydatain)
%MYFUN 此处显示有关此函数的摘要
%   此处显示详细说明
% a=coeffs(1);
% b=coeffs(2);
% c=coeffs(3);
% d=coeffs(4);
% x=xdata.x;
% y=xdata.y;

% ka=coeffs(1);
% kb=coeffs(2);
% kc=coeffs(3);
% kd=coeffs(4);
% kx=xdata.x;
% ky=xdata.y;

syms a b c d x y ydata
f=a+b./(c+x.*y./(d*x+(1-d)*y));
res=f-ydata;
res=sum(res.*res);
% res=norm(res);
% f_grad=gradient(res,[a b c d])
f_grad_syms=gradient(res,[a b c d]);

fnew=cell(1,length(f_grad_syms));
for i=1:length(f_grad_syms)  
       fnew{i}=eval(['@(a,b,c,d,x,y,ydata)',vectorize(f_grad_syms(i))]);
end  
   
a=coeffs(1);
b=coeffs(2);
c=coeffs(3);
d=coeffs(4);
x=xdata.x;
y=xdata.y;

%    classf=class(f{1})  
   f_grad_tmp=[fnew{1}(a,b,c,d,x,y,ydatain) fnew{2}(a,b,c,d,x,y,ydatain) ...
       fnew{3}(a,b,c,d,x,y,ydatain) fnew{4}(a,b,c,d,x,y,ydatain)];
   f_grad=sum(f_grad_tmp);

% % f_grad=str2double(f_grad_syms);
% f_grad=subs(f_grad_syms, [a, ka,b,kb,c,kc,d,kd,x,kx,y,ky])
  
% fun=inline(f_grad,'a','b','c','d','x','y')
% y=feval(fun,[])

% [f_grad]=full(AutoDiffJacobianAutoDiff(res,[a,b,c,d]));
end

