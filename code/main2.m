clear
clc
close all

xdata=[1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19];
ydata=[0.898 2.38 3.07 1.84 2.02 1.94 2.22 2.77 4.02...
    4.76 5.46 6.53 10.9 16.5 22.5 35.7 50.6 61.6 81.8];
mypline_tmp=pline(xdata,log(ydata));
mypline(1)=exp(mypline_tmp(2));
mypline(2)=mypline_tmp(1);
mypline

opts = optimoptions(@lsqcurvefit,'Algorithm','levenberg-marquardt');

x0=[0 0];
[X,FVAL,EXITFLAG,OUTPUT] = lsqcurvefit('myfun2_4fit',...
    x0,xdata,ydata,[],[],opts);
mylsqcurvefit=X

x0=[0 0 0 0];
[X,FVAL,EXITFLAG,OUTPUT] = lsqcurvefit('myfun2_4fit_2',x0,xdata,ydata);
mylsqcurvefit2=X


mypline_value=myfun2_4fit(mypline,xdata);
mylsqcurvefit_value=myfun2_4fit(mylsqcurvefit,xdata);
mylsqcurvefit2_value=myfun2_4fit_2(mylsqcurvefit2,xdata);

figure
plot(xdata,mypline_value)
hold on
plot(xdata,mylsqcurvefit_value)
plot(xdata,ydata)
plot(xdata,mylsqcurvefit2_value)
legend('pline','levmar','raw','2exps')

